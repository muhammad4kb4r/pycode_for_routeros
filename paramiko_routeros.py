import paramiko
import time

ssh_client2 = paramiko.SSHClient()
ssh_client2.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client2.connect('192.168.9.2', port=22, username='akbar2', password='akbar2')

print('Berhasil Login ke 192.168.9.2 \n')

stdin, stdout, stderr = ssh_client2.exec_command('system identity set name=ROUTER_MAKASSAR')

time.sleep(1)

stdin, stdout, stderr = ssh_client2.exec_command('ip firewall filter add chain=input protocol=icmp action=drop')

time.sleep(1)

stdin, stdout, stderr = ssh_client2.exec_command('ip firewal filter print')

output2 = stdout.readlines()

print('\n'.join(output2))

ssh_client2.close()

####################################################################################################################
ssh_client3 = paramiko.SSHClient()
ssh_client3.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client3.connect('192.168.9.3', port=22, username='akbar3', password='akbar3')

print('Berhasil Login ke 192.168.9.3 \n')

stdin, stdout, stderr = ssh_client3.exec_command('system identity set name=ROUTER_MAROS')

time.sleep(1)

stdin, stdout, stderr = ssh_client3.exec_command('ip route add dst-address=0.0.0.0/0 gateway=192.168.9.1')

time.sleep(1)

stdin, stdout, stderr = ssh_client3.exec_command('ip route print')

output3 = stdout.readlines()

print('\n'.join(output3))

ssh_client3.close()

####################################################################################################################
ssh_client4 = paramiko.SSHClient()
ssh_client4.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh_client4.connect('192.168.9.4', port=22, username='akbar4', password='akbar4')

print('Berhasil Login ke 192.168.9.4 \n')

stdin, stdout, stderr = ssh_client4.exec_command('system identity set name=ROUTER_GOWA')

time.sleep(1)

stdin, stdout, stderr = ssh_client4.exec_command('interface wireless add master-interface=wlan1 disabled=no ssid=WIFI4 mode=ap-bridge')

time.sleep(1)

stdin, stdout, stderr = ssh_client4.exec_command('interface wireless print')

output4 = stdout.readlines()

print('\n'.join(output4))

ssh_client4.close()